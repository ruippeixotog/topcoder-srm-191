public class BettingMoney {

	public int moneyMade(int[] amounts, int[] centsPerDollar, int finalResult) {
		int balance = 0;
		for(int i = 0; i < amounts.length; i++) {
			if(i == finalResult) {
				balance -= amounts[i] * centsPerDollar[i];
			} else {
				balance += amounts[i] * 100;
			}
		}
		return balance;
	}

}
